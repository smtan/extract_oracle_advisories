#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
THIS_PROG="$0"

if [ ! -e "$DIR/env.local" ] ; then
    echo "Please copy"
    echo "→ $DIR/env.local.example"
    echo "to"
    echo "→ $DIR/env.local"
    echo "and fill out the environment variables!"
    exit 1
fi

. "$DIR/env.local"

bundle exec $DIR/extract_oracle_advisories "$@"

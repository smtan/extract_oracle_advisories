# frozen_string_literal: true

require_relative 'lib/extract_oracle_advisories/version'

Gem::Specification.new do |spec|
  spec.name          = 'extract_oracle_advisories'
  spec.version       = ExtractOracleAdvisories::VERSION
  spec.authors       = ['Tan Shao Ming']
  spec.email         = ['smtan@gitlab.com']

  spec.summary       = 'Oracle advisory extractor'
  spec.description   = 'Oracle advisory extractor'
  spec.homepage      = 'https://gdk.test:3443/root/extract-oracle-advisories.git'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.6.0')

  spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gdk.test:3443/root/extract-oracle-advisories.git'
  spec.metadata['changelog_uri'] = 'https://gdk.test:3443/root/extract-oracle-advisories.git'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.glob('{exe,bin,lib}/**/*') + %w[README.md LICENSE.txt]
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'fileutils'
  spec.add_dependency 'optparse'
  spec.add_dependency 'yaml'
  spec.add_dependency 'git'
  spec.add_dependency 'gitlab'
  spec.add_dependency 'semver_dialects'
  spec.add_dependency 'securerandom'
  spec.add_dependency 'httparty'

  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'pry-byebug'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rspec-mocks'
  spec.add_development_dependency 'rspec-parameterized'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'solargraph'
end

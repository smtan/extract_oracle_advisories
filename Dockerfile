# FROM ruby:3-alpine AS builder
# Pinning to known working version of ruby:3-alpine
FROM ruby:3-alpine AS builder

COPY . /
RUN apk update && apk --no-cache add make gcc libc-dev git sqlite-dev bash \
    && gem install bundler \
    && bundle config set --local frozen 'true' \
    && bundle install

WORKDIR /
ENTRYPOINT /bin/bash

# frozen_string_literal: true

require 'logger'
require 'optparse'

require_relative 'log'
require_relative 'utils'

module ExtractOracleAdvisories
  # config module for AS
  module Config
    class Error < StandardError; end

    PARAMAP = {
      advisory_db: 'ADVISORY_DB',
      api_endpoint: 'API_ENDPOINT',
      advisory_db_api_token: 'ADVISORY_DB_API_TOKEN',
      advisory_db_project_id: 'ADVISORY_DB_PROJECT_ID'
    }.freeze

    # rubocop:disable Style/MutableConstant
    CONFIG = {
      advisory_db: Utils.default_env('ADVISORY_DB', ''),
      api_endpoint: Utils.default_env('API_ENDPOINT', ''),
      advisory_db_api_token: Utils.default_env('ADVISORY_DB_API_TOKEN', ''),
      advisory_db_project_id: Utils.default_env('ADVISORY_DB_PROJECT_ID', '')
    }
    # rubocop:enable Style/MutableConstant

    # print settings that are safe to print
    def self.show_config_safe
      safe_keys = %i[
        aports_db
      ]
      puts 'Config:'
      safe_keys.each do |key|
        puts format('  %<key>-25s %<value>s', key: key, value: CONFIG[key])
      end
    end

    def self.parse_args
      OptionParser.new do |opts|
        opts.banner = <<~BANNER

          Extract relevant advisory for Alpine

          Usage: extract [options]
        BANNER
        opts.on('--advisory_db ADVISORY_DB',
                'advisory db')
        opts.on('--api_endpoint API_ENDPOINT',
                'gitlab API endpoint')
        opts.on('--advisory_db_api_token ADVISORY_DB_API_TOKEN',
                'advisory db API token')
        opts.on('--advisory_db_api_token ADVISORY_DB_PROJECT_ID',
                'advisory db project id')
      end.parse!(into: CONFIG)

      # command-line arguments are stored as CONFIG['email-from'.to_sym] instead
      # of CONFIG[:email_from]. This transforms all parsed cmd-line args into the
      # underscore version
      CONFIG.each do |k, v|
        next unless k.to_s =~ /\w*-\w*/

        CONFIG[k.to_s.gsub('-', '_').to_sym] = v
      end

      errors = []
      errors << %i[
        advisory_db
        api_endpoint
        advisory_db_api_token
        advisory_db_project_id
      ].map do |s|
        "#{PARAMAP[s]} must be set" if CONFIG[s].to_s.empty?
      end
      errors.flatten!
      errors.compact!

      raise Error, "Configuration Error:\n\n#{errors.join("\n")}" unless errors.count.zero?

      CONFIG
    end
  end
end

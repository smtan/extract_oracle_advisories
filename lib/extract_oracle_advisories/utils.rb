# frozen_string_literal: true

require 'fileutils'
require 'extract_oracle_advisories/log'

module Utils
  class << self
    include ExtractOracleAdvisories::Logging
  end

  def self.default_env(key, default)
    val = ENV[key]
    # gitlab-ci quirk about variables - variables must be explicitly copied into
    # the variables for a new job. if the variable is undefined, the value of
    # the copied (empty) variable is '$VARIABLE'
    return default if ["$#{key}", nil, ''].include?(val)

    val
  end
end

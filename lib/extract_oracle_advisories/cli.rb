require 'extract_oracle_advisories/config'
require 'date'
require 'fileutils'
require 'git'
require 'gitlab'
require 'json'
require 'pathname'
require 'set'
require 'time'
require 'yaml'
require 'securerandom'
require 'uri'
require 'net/http'
require 'httparty'
require 'pry'

class NvdYamlGenerator
  def yaml_file_handle
    @yaml_file_handle
  end

  def write_yaml(obj)
    puts "write"
    super.write(obj)
  end
end

module ExtractOracleAdvisories
  module CLI
    class << self
      include Logging
    end
    class Error < StandardError; end

    def self.convertToOvalCriteria(criteria, os_version)
      ovalCriteria = {
        "Operator": criteria["operator"],
        "Criterias": [],
        "Criterions": [],
      }
      
      # criteria can contain nested criterias. Recursively loop through them.
      if criteria["criteria"]
        criteria["criteria"].each_with_index do |sub_criteria, index|
          ovalCriteria[:Criterias][index] = convertToOvalCriteria(sub_criteria, os_version)
        end
      end

      if criteria["criterion"]
        criteria["criterion"].each_with_index do |sub_criterion, index|
          # Redhat API does not include the OS version in the top level criterion. This is essential for trivy-db to match the os version to the package.
          # https://gitlab.com/gitlab-org/security-products/dependencies/trivy-db/-/blob/f498a19a857b78d3ea1b026050dbbfd081e54423/pkg/vulnsrc/oracle-oval/oracle-oval.go#L204-205
          if sub_criterion["comment"].include?("must be installed") 
            ovalCriteria[:Criterions][index] = {
              "Comment": "Oracle Linux #{os_version} is installed"
            }
          else
            # Rename neccessary for trivy-db to extract package and os details
            ovalCriteria[:Criterions][index] = {
              "Comment": sub_criterion["comment"].gsub("Red Hat Enterprise Linux","Oracle Linux")
            }
          end
        end
      end
      return ovalCriteria
    end

    def self.extract_advisory(url, year, extract_dir)
      # sleeping as it seems that requests takes longer without a pause.
      sleep 0.1

      log.info("Fetching advisory #{url}")
      requestStartTime = Time.now()
      res = HTTParty.get(url)
      requestEndTime = Time.now()
      json_output = JSON.parse(res.body)
      
      os_version = json_output["oval_definitions"]["definitions"]["definition"]["metadata"]["affected"]["platform"][0].split("Red Hat Enterprise Linux ")[1]
      advisory_id = ""

      # Parse response in Oracle Oval format that Trivy requires. Eg: https://gitlab.com/gitlab-org/secure/vulnerability-research/advisories/vuln-list-mirror/-/blob/main/oval/oracle/2023/ELSA-2023-0005.json
      parsed_advisory = {
        "Title": json_output["oval_definitions"]["definitions"]["definition"]["metadata"]["title"],
        "Description": json_output["oval_definitions"]["definitions"]["definition"]["metadata"]["description"],
        "Platform": json_output["oval_definitions"]["definitions"]["definition"]["metadata"]["affected"]["platform"],
        "References": [],
        "Criteria": convertToOvalCriteria(json_output["oval_definitions"]["definitions"]["definition"]["criteria"],os_version),
        "Severity": json_output["oval_definitions"]["definitions"]["definition"]["metadata"]["advisory"]["severity"],
        "Cves": [],
        "Issued": {
          "Date": json_output["oval_definitions"]["definitions"]["definition"]["metadata"]["advisory"]["issued"]["date"]
        },
      }
      for reference in json_output["oval_definitions"]["definitions"]["definition"]["metadata"]["reference"]
        if (reference["source"] === "RHSA")
          advisory_id = reference["ref_id"].gsub(":","-")
        end
        parsed_advisory[:References].push({
          "Source": reference["source"],
          "URI": reference["ref_url"],
          "ID": reference["ref_id"]
        })
      end

      for cve in json_output["oval_definitions"]["definitions"]["definition"]["metadata"]["advisory"]["cve"]
        parsed_advisory[:Cves].push({
          "Impact": "",
          "Href": "",
          "ID": cve
        })
      end

      # Save JSON file
      target_dir = File.join(extract_dir, "oval", "oracle", year.to_s())
      filename = "#{advisory_id}.json"
      target_file = File.join(target_dir, filename)
      FileUtils.mkdir_p(target_dir) unless Dir.exist?(target_dir)
      log.info("write #{target_file}")
      File.write(target_file, JSON.pretty_generate(parsed_advisory))

      parsingDuration = Time.now()-requestEndTime
      log.info("Fetched advisory in #{requestEndTime-requestStartTime}, Parsed in: #{parsingDuration}\n")
      return parsed_advisory
    end

    def self.get_advisories_url_in_year(year)
      list_url   = "https://access.redhat.com/labs/securitydataapi/oval.json?"

      log.info("Fetching advisories for #{year}")
      after = "#{year}-01-01"
      before = "#{year+1}-01-01"
      advisories_url = []
      page = 1

      loop do 
        page_url = "#{list_url}page=#{page}&after=#{after}&before=#{before}&per_page=500"
        
        log.info("Fetching page #{page}: #{page_url}")
        res = HTTParty.get(page_url)
        # Todo handle error, possibly retry

        json_output = JSON.parse(res.body)
        # Stop fetching if page returns 0 advisories
        if json_output === []
          break
        end

        json_output.each { |advisory| advisories_url.push(advisory["resource_url"]) }

        page += 1
      end
      return advisories_url
    end

    def self.extract_advisories(extract_dir)
      year_list = [2023, 2022, 2021, 2020, 2019, 2018, 2017, 2016, 2015, 2014, 2013, 2012, 2011, 2010, 2009, 2008, 2007]

      for year in year_list do
        advisories_url = get_advisories_url_in_year(year)
        if advisories_url.length > 0
          for advisory_url in advisories_url
            extract_advisory(advisory_url, year, extract_dir)
          end
        end
      end
    end

    def self.update(target_repo)
      conf = ExtractOracleAdvisories::Config::CONFIG
      api_token = conf[:advisory_db_api_token]
      api_endpoint = conf[:api_endpoint]
      project_id = conf[:advisory_db_project_id]

      timestamp = Time.now.to_date.strftime('%Y%d%m')
      description = "Sync #{timestamp}"

      branchname = File.join("sync-#{timestamp.to_i}")

      mr_labels = ['vulnerability research::advisory',
             'group::vulnerability research',
             'devops::secure', 
             'section::securedefend',
             'advisory::automerge',
             'Category:Vulnerability Database'].freeze

      target_repo.config('user.email', 'gitlab-bot@gitlab.com')
      target_repo.config('user.name', 'GitLab Bot')

      remote_branch = false
      begin
        target_repo.fetch('origin', { ref: "#{branchname}:#{branchname}" })
        remote_branch = target_repo.is_branch?(branchname)
      rescue Git::GitExecuteError => e
        log.info("remote branch #{branchname} not present yet")
      end

      if target_repo.is_branch?(branchname) || remote_branch
        log.info("remote or local branch #{branchname} already present ... skipping")
        return
      end

      begin
        target_repo.branch(branchname).checkout
        target_repo.add(all: true)

        if target_repo.status.changed.empty? && target_repo.status.added.empty?
          log.info("nothing to commit")
          return 
        end

        target_repo.commit("sync #{timestamp}")

        log.info("pushing changes to #{branchname}")

        target_repo.push('origin', target_repo.current_branch)
      rescue Git::GitExecuteError => e
        log.error("git error #{e}")
      end

      client = Gitlab.client(
        endpoint: api_endpoint,
        private_token: api_token
      )

      create_payload = {
        source_branch: branchname,
        description: description,
        target_branch: 'main',
        remove_source_branch: true,
        labels: mr_labels
      }

      log.info("preparing MR for '#{branchname}'")

      begin
        mr = client.create_merge_request(project_id,
                                         description,
                                         create_payload)
        log.info("mr: #{mr.id} created")
      rescue Net::ReadTimeout => e
        log.error("create mr attempt #{retries} timed out #{e}")
      rescue Gitlab::Error::MethodNotAllowed => e
        log.error("unable to create MR #{e}")
      end
    end

    def self.run
      begin
        ExtractOracleAdvisories::Config.parse_args
      rescue ExtractOracleAdvisories::Config::Error => e
        log.error(e.to_s)
        exit 1
      end
      ExtractOracleAdvisories::Config.show_config_safe
      conf = ExtractOracleAdvisories::Config::CONFIG

      advisory_db = conf[:advisory_db]
      advisory_directory = '/tmp/oracle-advisories'

      target_repo = nil

      Dir.chdir("/tmp") do |repo|
        log.info("clone oracle-advisories")
        unless Dir.exist?('oracle-advisories')
          target_repo = Git.clone(advisory_db, 'oracle-advisories', {:log => logger, depth: 1})
        else 
          target_repo = Git.open(advisory_directory)
        end
      end

      if target_repo.nil? 
        log.error("Git repo could not be created")
        exit(1)
      end

      # Fetch oracle advisories
      start_time = Time.now()
      log.info("Start fetch oracle advisories at #{start_time}")

      extract_advisories(advisory_directory)

      end_time = Time.now()
      log.info("Completed fetch oracle advisories at #{end_time}, duration #{end_time-start_time}")

      update(target_repo)
    end
  end
end

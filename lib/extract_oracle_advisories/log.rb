# frozen_string_literal: true

require 'logger'

# Logging class
module ExtractOracleAdvisories
  module Logging
    class << self
      attr_writer :log_level

      def log_level
        @log_level || Logger::INFO
      end
    end

    attr_accessor :logger

    def self.get_logger(logname, level = Logger::INFO)
      logger = Logger.new($stdout)
      logger.level = level
      orig_formatter = Logger::Formatter.new
      logger.formatter = proc { |severity, datetime, progname, msg|
        orig_formatter.call(severity,
                            datetime,
                            progname,
                            "#{logname}: #{msg}")
      }
      logger
    end

    def log
      logname = @log_name || self.class.name
      @logger = Logging.get_logger(logname, Logging.log_level) if @logger.nil?
      @logger
    end
  end
end

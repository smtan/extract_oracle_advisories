# Extract Oracle Advisories

## Development

This project can be built with `bundle install`. The test cases can be executed
with `bundle exec rspec spec`.

### `bin/run_local.sh` and `bin/.env`

Copy the `bin/.env.example` to `bin/.env` and update all variables to correct
values. 

Run the script with your environment variables loaded with the `bin/run_local.sh`
script. 

The configuration paramters are explained in more detail below:

``` bash
# Git target repository to store the aports advisories
export ADVISORY_DB=""
# GitLab API token used for creating the MRs
export ADVISORY_DB_API_TOKEN=""
# Project ID
export ADVISORY_DB_PROJECT_ID=""
```

